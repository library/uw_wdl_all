<?php

function responsive_bartik_preprocess_html(&$variables)
{
  // Add variables for path to theme.
  $variables['base_path'] = base_path();
  $variables['path_to_resbartik'] = drupal_get_path('theme', 'responsive_bartik');

  /*
  // Add local.css stylesheet
  if (file_exists(drupal_get_path('theme', 'responsive_bartik') . '/css/local.css')) {
    drupal_add_css(drupal_get_path('theme', 'responsive_bartik') . '/css/local.css',
      array('group' => CSS_THEME, 'every_page' => TRUE));
  }
  */
  if (drupal_is_front_page()) {
    //drupal_add_css(path_to_theme() . "/foo.css", 'theme','all');
    drupal_add_css(drupal_get_path('theme', 'responsive_bartik') . '/css/custom/front_page.css',array('group' => CSS_THEME, 'every_page' => FALSE));
    drupal_add_css(drupal_get_path('theme', 'responsive_bartik') . '/css/custom/slider.css',array('group' => CSS_THEME, 'every_page' => FALSE));
    drupal_add_css(drupal_get_path('theme', 'responsive_bartik') . '/css/custom/screen_front.css',array('group' => CSS_THEME, 'every_page' => FALSE));
  }

  // Add body classes if certain regions have content.
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])
  ) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])
  ) {
    $variables['classes_array'][] = 'footer-columns';
  }
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function responsive_bartik_process_html(&$variables)
{
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function responsive_bartik_process_page(&$variables)
{
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function responsive_bartik_preprocess_maintenance_page(&$variables)
{
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'responsive_bartik') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function responsive_bartik_process_maintenance_page(&$variables)
{
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function responsive_bartik_preprocess_node(&$variables)
{
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

/**
 * Override or insert variables into the block template.
 */
function responsive_bartik_preprocess_block(&$variables)
{
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function responsive_bartik_menu_tree($variables)
{
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function responsive_bartik_field__taxonomy_term_reference($variables)
{
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}


//Custom modification of breadcrumbs by Lathangi Ganesh
function responsive_bartik_breadcrumb($variables)
{
  $breadcrumb = $variables['breadcrumb'];
  unset( $breadcrumb[1] );
  $breadcrumb = array_merge($breadcrumb);

  $word = "";
  $path = current_path();
  $path_alias = drupal_lookup_path('alias',$path);
  $string = explode("/", $path_alias);
  $word_un = "";
  if(!empty($string[1]))
  {
    $word_un = $string[1];
    $word = str_replace("-", " ", $string[1]);
  }
  $uc_first = ucfirst($word);

  $name_space = "";
  $url = $_GET['q'];
  $url_ex = explode(":", $url);
  if(!empty($url_ex[1]))
  {
    $name_space = $url_ex[1];
  }
  $breadcrumb = array_merge($breadcrumb);
  $link = '<a href="/uwdl-'.$name_space.'/'.$word_un.'">'.$uc_first.'</a>';

  $crumbs = "";

  if (!empty($breadcrumb))
  {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $crumbs = '<div class="breadcrumb">';

    /*
    foreach ($breadcrumb as $key => $value) {
      if($value == '<a href="/uwdl-uwdl/waterloo-digital-library">Waterloo Digital Library</a>')
      {
        array_push($breadcrumb,$link);
      }
    }
    */

    unset( $breadcrumb[1] );
    $breadcrumb = array_merge($breadcrumb);

    foreach ($breadcrumb as $i => $value) {
      $crumbs .= '<span class="breadcrumb-' . $i;
      if ($i == 0)
      {
        $crumbs .= ' first';
      }
      $crumbs .=  '">' . $breadcrumb[$i] . '</span> &raquo; ';
    }

    //Fix for compound object breadcrumbs(repeating many times)
    $orig_title = drupal_get_title();
    $tmp_arr = explode("-", $orig_title);
    $tmp_title = $tmp_arr[0];
    if(!empty($tmp_title))
    {
      $final_title = $tmp_title;
    }else{
      $final_title = $orig_tit;
    }
    $crumbs .= '<span>'. ucfirst($final_title) .'</span></div>';

    //$crumbs .= '<span>'. ucfirst(drupal_get_title()) .'</span></div>';
    return $crumbs;
  }
  else
  {
    //Breadcrumb settings for Home page
    $site_path = variable_get('file_public_path', base_path());
    $crumbs = '<span class="breadcrumb-0 first" id="bid"><a href="'.$site_path.'">Home</a></span>';
    return $crumbs;
  }
}// End of function breadcrumb

