/**
 * This js is responsible for all the custom modifications
 * Display date slider before facets,
 * Removing title for Advanced search page -- The title comes twice
 * Modication for List and Grid view for our custom need
 * Written by Lathangi Ganesh March 09, 2018
 */

jQuery(document).ready(function($) {
  /**********************To display Date slider before facets -- They are in the same block so it cannot be controlled by blocks****************/
  $( ".islandora-solr-facet-wrapper:contains('Date')" ).insertBefore( "#block-islandora-solr-basic-facets .content" );

  /******************************Modification for Advanced Search page title**************/
  //var pageURL = $(location). attr("href");
  var pageURL = window.location.href ;
  if(pageURL == "http://libislandstg01.private.uwaterloo.ca/content/wdl-advanced-search")
  {
    $('#page-title').remove();
  }

  /********************************Modification for  List and Grid View************************/
  //get current URL
  var current_url = window.location.href;
  //current_url = "http://libislandstg01.uwaterloo.ca/uwdl-uwa/university-waterloo-archives?display=list"
  //current_url = "http://libislandstg01.uwaterloo.ca/uwdl-uwa/university-waterloo-archives?display=grid";
  var n = check_words(current_url);
  if(n == 1)
  {
    var sp = current_url.split("?display=");
    var word = sp[1];
    var mod = word.slice(0,4);
  }else{
    var mod = "apply";
  }

  if((mod != 'grid') && (mod != 'list'))
  {
    var child_records = 0;
    //var grid = $( "span ul li a:contains('Grid view')" ).hasClass('active');
    var list = $( "span ul li a:contains('List view')" ).hasClass('active');

    $( "dl" ).each(function(i) {
      var class_name = $("dl").attr("class");
      var class_name_length = class_name.length;
      if(class_name_length > 30)
      {
        child_records = i + 1;
      }
    });

    //Check for more 10 records in page and still in List View
    if((child_records > 10) && (list == true))
    {
      window.location.href = current_url + '?display=grid';
      /*
      var tot = 0;
      var cnt = 0;

      //check for the description length
      $(" .dc-description").each(function(m) {
        var desc = $(this).text();
        tot = tot + 1;
        var str_desc = $(this).text().toString();
        var str_len = str_desc.length;

        if(str_len > 150)
        {
          cnt = cnt + 1;
        }
      });

      //Get the percentage of the records that has good lenth of description
      var percent = get_percentage(cnt,tot);
      //If less than 85% of the records that is not having proper description are displayed as grid view
      if(percent < 85)
      {
        window.location.href = current_url + '?display=grid';
      }
      */
    }
  }


  //Advanced search block is visible when the search text box is clicked
  $('#edit-islandora-simple-search-query').click(function(){
    $("#block-block-5").css("visibility","visible");
  });

  //Advanced search block is invisible by clicking main wrapper or featured area
  $("#featured, #main-wrapper").click( function(){
    $('#block-block-5').css("visibility","hidden");
  });



//Get the percentage of good records with proper description
function get_percentage(cnt,tot)
{
  var pcent = cnt/tot * 100;
  return pcent;
}

function check_words(c_url)
{
  var c_url = current_url.toString();
  var n1 = c_url.search("display=grid");
  var n2 = c_url.search("display=list");
  if(n1 > 0 || n2 > 0)
  {
    return 1;
  }else{
    return -1;
  }
}


});//End of JQuery

