/**
 * This js is responsible for icons for play and pause buttons
 * Written by Lathangi Ganesh March 09, 2018
 */

(function($) {

  var play = '<button style="font-size:17px"><i class="fa fa-play"></i></button>';
  var pause = '<button style="font-size:17px"><i class="fa fa-pause"></i></button>';

  // Theme the resume control.
  Drupal.theme.prototype.viewsSlideshowControlsPause = function () {
    $('#views_slideshow_controls_text_pause_slide_show-block_1').html(play);
  };

  // Theme the pause control.
  Drupal.theme.prototype.viewsSlideshowControlsPlay = function () {
    $('#views_slideshow_controls_text_pause_slide_show-block_1').html(pause);
  };

})(jQuery);

