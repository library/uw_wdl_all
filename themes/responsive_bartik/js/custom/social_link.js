/**
 * This js is responsible for social media buttons
 * Written by Lathangi Ganesh March 09, 2018
 */

(function($) {

  $( document ).ready(function() {
    var link = document.createElement( "link" );
    link.href = "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css";
    link.type = "text/css";
    link.rel = "stylesheet";
    link.media = "screen,print";
    document.getElementsByTagName( "head" )[0].appendChild( link );

    var current_location = window.location.href;
    var social_link = "<div class=\"social_link\"><div class=\"uw-digital\"><ul class=\"share\">";
    social_link += "<li><a class=\"social-icon\" aria-label=\"Share this on Facebook\" href=\"http://www.facebook.com/sharer.php?u=" + current_location + "\">" + "<i class=\"fa ic fa-facebook\" aria-hidden=\"true\"></i></a></li>";
    social_link += "<li><a class=\"social-icon\" aria-label=\"Share this on Twitter\" href=\"https://twitter.com/share?url=" + current_location + "\">" + "<i class=\"fa ic fa-twitter\" aria-hidden=\"true\"></i></a></li>";
    social_link += "<li><a class=\"social-icon\" aria-label=\"Share this via email\" href=\"mailto:?subject=UWaterloo Digital Library shared link&body=" + current_location + "\">" + "<i class=\"fa ic fa-envelope\" aria-hidden=\"true\"></i></a></li>";
    social_link += "<li><a class=\"social-icon\" aria-label=\"Share this on Instagram\" href=\"https://instagram.com/submit?url=" + current_location + "\">" + " <i class=\"fa ic fa-instagram\" aria-hidden=\"true\"></i></a></li>";
    social_link += "<li><a class=\"social-icon\" aria-label=\"Share this on LinkedIn\" href=\"http://www.linkedin.com/shareArticle?url=" + current_location + "\">" + "<i class=\"fa ic fa-linkedin\" aria-hidden=\"true\"></i></a></li></ul></div></div>";
    $("#page-title").append(social_link);
   });

})(jQuery);


